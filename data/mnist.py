import logging
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import pickle
IMG_DIM = 28
logger = logging.getLogger(__name__)

def get_mixed_train(*args):
    """Get training dataset for mixed MNIST"""
    X = np.load(r"./data/data.npk", allow_pickle=True)
    y = np.load(r"./data/y.npk", allow_pickle=True)
    return X, y

def get_train(*args):
    """Get training dataset from all number 4 of MNIST"""
    data = np.load(r"./data/mnist_4.npz", allow_pickle=True)
    X = data['x_train']
    y = data['y_train']
    return X, y

def get_test(*args):
    data = np.load(r"./data/mnist_4.npz", allow_pickle=True)
    X = data['x_test']
    y = data['y_test']
    return X, y

def get_train_mix(*args):
    """Get training dataset for mixed 4 and other numbers MNIST"""
    data = np.load(r"./data/mnist_4_mix.npz", allow_pickle=True)
    X = data['x_train']
    y = data['y_train']
    return X, y

def get_test_mix(*args):
    data = np.load(r"./data/mnist_4_mix.npz", allow_pickle=True)
    X = data['x_test']
    y = data['y_test']
    return X, y


def get_test(label=0, scale=False, *args):
    """Get testing dataset for KDD 10 percent"""
    return _get_adapted_dataset("test", scale)

def get_valid(label=0, scale=False, *args):
    """Get validation dataset for KDD 10 percent"""
    return None

def get_shape_input():
    """Get shape of the dataset for KDD 10 percent"""
    return (None, IMG_DIM, IMG_DIM, 1)

def get_shape_label():
    """Get shape of the labels in KDD 10 percent"""
    return (None,)

def get_anomalous_proportion():
    return 0.2


def _get_dataset(scale):
    """ Gets the basic dataset
    Returns :
            dataset (dict): containing the data
                dataset['x_train'] (np.array): training images shape
                (?, 120)
                dataset['y_train'] (np.array): training labels shape
                (?,)
                dataset['x_test'] (np.array): testing images shape
                (?, 120)
                dataset['y_test'] (np.array): testing labels shape
                (?,)
    """
    try:
        with open('data/kdd_processed.pkl', 'rb') as f:
            print("Loading kdd data from pkl file.")
            proc_data = pickle.load(f)
            x_train = proc_data['x_train']
            y_train = proc_data['y_train']
            x_valid = proc_data['x_valid']
            y_valid = proc_data['y_valid']
            x_test = proc_data['x_test']
            y_test = proc_data['y_test']
    except IOError:
        col_names = _col_names()
        df = pd.read_csv("data/kddcup.data_10_percent_corrected", header=None, names=col_names)
        text_l = ['protocol_type', 'service', 'flag', 'land', 'logged_in', 'is_host_login', 'is_guest_login']

        for name in text_l:
            _encode_text_dummy(df, name)

        labels = df['label'].copy()
        labels[labels != 'normal.'] = 0
        labels[labels == 'normal.'] = 1

        df['label'] = labels

        df_train = df.sample(frac=0.5, random_state=42)
        df_test = df.loc[~df.index.isin(df_train.index)]
        df_valid = df_train.sample(frac=0.1, random_state=42)
        # df_train = df_train.loc[~df_train.index.isin(df_valid.index)]

        x_train, y_train = _to_xy(df_train, target='label')
        x_valid, y_valid = _to_xy(df_valid, target='label')
        x_test, y_test = _to_xy(df_test, target='label')

        y_train = y_train.flatten().astype(int)
        y_valid = y_valid.flatten().astype(int)
        y_test = y_test.flatten().astype(int)
        x_train = x_train[y_train != 1]
        y_train = y_train[y_train != 1]
        x_valid = x_valid[y_valid != 1]
        y_valid = y_valid[y_valid != 1]
        # storing processed data
        proc_data = {'x_train':x_train,
                     'y_train':y_train,
                     'x_valid':x_valid,
                     'y_valid':y_valid,
                     'x_test':x_test,
                     'y_test':y_test}
        with open('data/kdd_processed.pkl', 'wb') as f:
            pickle.dump(proc_data, f, pickle.HIGHEST_PROTOCOL)
            print("Dumped processed kdd to data/kdd_processed.pkl")

    if scale:
        print("Scaling KDD dataset")
        scaler = MinMaxScaler()
        scaler.fit(x_train)
        x_train = scaler.transform(x_train)
        x_valid = scaler.transform(x_valid)
        x_test = scaler.transform(x_test)

    dataset = {}
    dataset['x_train'] = x_train.astype(np.float32)
    dataset['y_train'] = y_train.astype(np.float32)
    dataset['x_valid'] = x_valid.astype(np.float32)
    dataset['y_valid'] = y_valid.astype(np.float32)
    dataset['x_test'] = x_test.astype(np.float32)
    dataset['y_test'] = y_test.astype(np.float32)

    return dataset

def _get_adapted_dataset(split, scale):
    """ Gets the adapted dataset for the experiments

    Args :
            split (str): train or test
    Returns :
            (tuple): <training, testing> images and labels
    """
    # print("_get_adapted",scale)
    dataset = _get_dataset(scale)
    key_img = 'x_' + split
    key_lbl = 'y_' + split

    if split == 'test':
        dataset[key_img], dataset[key_lbl] = _adapt_ratio(dataset[key_img],
                                                    dataset[key_lbl])

    return (dataset[key_img], dataset[key_lbl])

def _encode_text_dummy(df, name):
    """Encode text values to dummy variables(i.e. [1,0,0],[0,1,0],[0,0,1]
    for red,green,blue)
    """
    dummies = pd.get_dummies(df.loc[:,name])
    for x in dummies.columns:
        dummy_name = "{}-{}".format(name, x)
        df.loc[:, dummy_name] = dummies[x]
    df.drop(name, axis=1, inplace=True)

def _to_xy(df, target):
    """Converts a Pandas dataframe to the x,y inputs that TensorFlow needs"""
    result = []
    for x in df.columns:
        if x != target:
            result.append(x)
    dummies = df[target]
    return df.as_matrix(result).astype(np.float32), dummies.as_matrix().flatten().astype(int)

def _col_names():
    """Column names of the dataframe"""
    return ["duration","protocol_type","service","flag","src_bytes",
    "dst_bytes","land","wrong_fragment","urgent","hot","num_failed_logins",
    "logged_in","num_compromised","root_shell","su_attempted","num_root",
    "num_file_creations","num_shells","num_access_files","num_outbound_cmds",
    "is_host_login","is_guest_login","count","srv_count","serror_rate",
    "srv_serror_rate","rerror_rate","srv_rerror_rate","same_srv_rate",
    "diff_srv_rate","srv_diff_host_rate","dst_host_count","dst_host_srv_count",
    "dst_host_same_srv_rate","dst_host_diff_srv_rate","dst_host_same_src_port_rate",
    "dst_host_srv_diff_host_rate","dst_host_serror_rate","dst_host_srv_serror_rate",
    "dst_host_rerror_rate","dst_host_srv_rerror_rate","label"]

def _adapt_ratio(x, y, rho=0.2):
    """Adapt the ratio of normal/anomalous data"""

    # Normal data: label =0, anomalous data: label =1

    rng = np.random.RandomState(42) # seed shuffling

    inliersx = x[y == 0]
    inliersy = y[y == 0]
    outliersx = x[y == 1]
    outliersy = y[y == 1]

    size_outliers = outliersx.shape[0]
    inds = rng.permutation(size_outliers)
    outliersx, outliersy = outliersx[inds], outliersy[inds]

    size_x = inliersx.shape[0]
    out_size_x = int(size_x*rho/(1-rho))

    out_sample_x = outliersx[:out_size_x]
    out_sample_y = outliersy[:out_size_x]

    x_adapted = np.concatenate((inliersx,out_sample_x), axis=0)
    y_adapted = np.concatenate((inliersy,out_sample_y), axis=0)

    size_x = x_adapted.shape[0]
    inds = rng.permutation(size_x)
    x_adapted, y_adapted = x_adapted[inds], y_adapted[inds]

    return x_adapted, y_adapted
