from __future__ import division
import PIL.Image as Image
from data import ImShow as I
import numpy as np
import os
import glob
import shutil
import tensorflow as tf
import argparse
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, auc, precision_recall_fscore_support
from collections import Counter
from utils.evaluations import get_percentile
import importlib
# not print warning messages
from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False

IMG_DIM = 28

def alad(args):

    data = importlib.import_module("data.{}".format(args.dataset))
    x_train, y_train = data.get_train()
    x_test_mix, y_test_mix = data.get_test_mix()
    print("x_train.shape={}".format(x_train.shape))
    print("x_test_mix.shape={}".format(x_test_mix.shape))

    current_dir = os.getcwd()
    folder ="train_logs/anomaly_detection/{}/ALAD".format(args.dataset)
    last_folder ="train_logs/anomaly_detection/{}/ALAD_last".format(args.dataset)

    # backup last result
    train = False
    if args.train_and_test=='train_and_test':
        train = True
        if not os.path.isdir(last_folder):
            os.makedirs(last_folder)
        bak_files = glob.glob(r'{}/*.png'.format(folder))
        for bak_file in bak_files:
            shutil.copy(bak_file, "{}/".format(last_folder))

    if not os.path.isdir(folder):
        os.makedirs(folder)

    with tf.Graph().as_default():
        config = tf.ConfigProto()
        config.gpu_options.visible_device_list= args.gpu_id
        config.gpu_options.allow_growth = True
        with tf.Session(config=config) as sess:
            BiGAN = importlib.import_module('model.BasicBiGAN.BiGAN')
            bGan = BiGAN.BiGAN(sess=sess,
                logdir=folder,
                dataset=args.dataset,
                enable_early_stop=args.enable_early_stop)

            if train:
                bGan.fit(sess=sess, X=x_train, nb_epochs=args.nb_epochs,
                    verbose=args.verbose)
            else:
                bGan.restore(sess=sess)

            real_anomaly = y_test_mix
            if args.dataset == 'mnist':
                real_anomaly = (y_test_mix != 4) * 1

            scores_ch, scores_l1, scores_l2, scores_fm = bGan.evaluate(sess=sess,
                testx=x_test_mix)
            analyze_score("ch", scores_ch, real_anomaly, args.dataset)
            analyze_score("l1", scores_l1, real_anomaly, args.dataset)
            analyze_score("l2", scores_l2, real_anomaly, args.dataset)
            analyze_score("fm", scores_fm, real_anomaly, args.dataset)

            if args.dataset == 'mnist' and train:
                rec_x = bGan.getRecon(sess=sess, X=x_test_mix)
                os.chdir(folder)
                Image.fromarray(I.tile_raster_images(X=rec_x,
                    img_shape=(IMG_DIM, IMG_DIM),
                    tile_shape=(20, 20),
                    tile_spacing=(1,1))).save(r'R.png')

def analyze_score(method, scores, true_labels, dataset):
    print("\n===========Summary, method={}, dataset={}====".
        format(method, dataset))
    scores = np.array(scores)
    fpr, tpr, _ = roc_curve(true_labels, scores)
    roc_auc = auc(fpr, tpr) # compute area under the curve
    per = get_percentile(scores, dataset)
    print("percentile={}".format(per))
    #mean_l = np.mean(scores)
    #std_l = np.std(scores)
    #pred_anomaly = (scores - mean_l > std_l) * 1
    y_pred = (scores >= per)*1
    print("y_true={}".format(Counter(true_labels)))
    print("y_pred={}".format(Counter(y_pred)))
    precision, recall, f1, _ = precision_recall_fscore_support(
        true_labels.astype(int), y_pred.astype(int), average='binary')
    tn, fp, fn, tp = confusion_matrix(true_labels, y_pred).ravel()
    accuracy = (tp+tn)/(tp+tn+fp+fn)
    tpr = tp/(tp + fn) # recall
    tnr = tn/(tn + fp)
    print("tn={}, fp={}, fn={}, tp={}| tpr={} tnr={}".
        format(tn, fp, fn, tp, tpr, tnr))
    print("method ={}, accuracy={}, roc_auc={}, precision={}, "
        "recall={}, f1={}".
        format(method, accuracy, roc_auc, precision, recall, f1))
    print("===========Summary==============\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'train_and_test',
        nargs='?',
        choices=['train_and_test','train', 'test'],
        help='Choose either train or test or both')
    parser.add_argument(
        '--verbose',
        help='Verbosity with printout debug message',
        action='store_true')
    parser.add_argument(
        '--enable_early_stop',
        help='Enable early stop during training GAN',
        action='store_true')
    parser.add_argument(
        '--nb_epochs',
        type=int,
        help='Number of epochs to iterate. Default=1.',
        default=1)
    parser.add_argument(
        '--random_seed',
        type=int,
        help='Random seed number. Default=42.',
        default=42)
    parser.add_argument(
        '--gpu_id',
        type=str,
        help='GPU ID. Default=0.',
        default='0')
    parser.add_argument(
        '--dataset',
        type=str,
        help='Data set name. Default is mnist.',
        default='mnist')

    args = parser.parse_args()
    alad(args)
