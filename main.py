import PIL.Image as Image
from data import ImShow as I
import numpy as np
import numpy.linalg as nplin
import importlib
import time
import glob
import os
import shutil
import tensorflow as tf
# not print warning messages
from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False

from model import RGAN as RGAN
import argparse
from utils.evaluations import save_results
from utils.evaluations import get_percentile
from collections import Counter

IMG_DIM = 28

def RobustGAN(X, y, lamda, norm_s='L1', dataset='mnist',
    train=False, test=False,
    gpu_id='0',
    inner_iteration=100, outer_iteration=10,
    re_init=False,
    verbose=False, random_seed=42, enable_early_stop=False):

    current_dir = os.getcwd()
    folder ="train_logs/anomaly_detection/{}/{}RGAN/{}".\
        format(dataset, args.norm_s, lamda)
    last_folder ="train_logs/anomaly_detection/{}/{}RGAN/{}_last".\
        format(dataset, args.norm_s, lamda)

    # backup last result
    if train:
        if not os.path.isdir(last_folder):
            os.makedirs(last_folder)
        bak_files = glob.glob(r'{}/*.png'.format(folder))
        bak_files.extend(glob.glob(r'{}/*.npk'.format(folder)))
        for bak_file in bak_files:
            shutil.copy(bak_file, "{}/".format(last_folder))

    if not os.path.isdir(folder):
        os.makedirs(folder)
    #else:
        #if train:
        #    shutil.rmtree(folder)
        #    os.makedirs(folder)
    os.chdir(folder)
    with tf.Graph().as_default():
        config = tf.ConfigProto()
        config.gpu_options.visible_device_list= gpu_id
        config.gpu_options.allow_growth = True
        with tf.Session(config=config) as sess:
            if dataset == 'mnist':
                image_X = Image.fromarray(I.tile_raster_images(
                    X=X,
                    img_shape=(IMG_DIM, IMG_DIM),
                    tile_shape=(20,20),
                    tile_spacing=(1,1)))
                image_X.save(r"X.png")

            rgan = RGAN.RGAN(sess=sess, logdir='.',
                lambda_=lamda*X.shape[0],
                norm_s = norm_s,
                dataset=dataset,
                enable_early_stop=enable_early_stop)
            if train:
                L, S = rgan.fit(X=X, sess=sess,
                    inner_iteration=inner_iteration,
                    iteration=outer_iteration,
                    verbose=verbose)
                R = rgan.getRecon(sess=sess, X=X)
                H = rgan.transform(sess=sess, X=X)
                if dataset == 'mnist':
                    Image.fromarray(I.tile_raster_images(X=S,
                        img_shape=(IMG_DIM, IMG_DIM),
                        tile_shape=(20, 20),
                        tile_spacing=(1,1))).save(r'{}S.png'.format(norm_s))
                    Image.fromarray(I.tile_raster_images(X=R,
                        img_shape=(IMG_DIM, IMG_DIM),
                        tile_shape=(20, 20),
                        tile_spacing=(1,1))).save(r'{}R.png'.format(norm_s))
                    Image.fromarray(I.tile_raster_images(X=L,
                        img_shape=(IMG_DIM, IMG_DIM),
                        tile_shape=(20, 20),
                        tile_spacing=(1,1))).save(r'{}L.png'.format(norm_s))
                S.dump("{}S.npk".format(norm_s))
                R.dump("{}R.npk".format(norm_s))
                L.dump("{}L.npk".format(norm_s))
                H.dump("{}H.npk".format(norm_s))
                #l1S = [nplin.norm(S[i], 1) for i in range(S.shape[0])]
                l1S = np.sum(np.abs(S), axis=-1) # much faster
            else:
                # load model from folder
                rgan.restore(sess)
                S, l1S = rgan.get_sparse(X, sess)

            scores_ch, scores_l1, scores_l2, scores_fm = \
                rgan.evaluate(sess=sess, testx=X)

            real_anomaly = y
            if dataset == 'mnist':
                # Each number differs from 4 is anomaly
                # 1 is anomaly (positive task), 0 is normal sample
                real_anomaly = (y != 4)*1

            os.chdir(current_dir)

            loss = scores_ch + lamda * l1S
            analyze_loss(lamda, 'ch', loss, real_anomaly, dataset, train,
                norm_s, random_seed, outer_iteration, inner_iteration,
                enable_early_stop)

            loss = scores_l1 + lamda * l1S
            analyze_loss(lamda, 'l1', loss, real_anomaly, dataset, train,
                norm_s, random_seed, outer_iteration, inner_iteration,
                enable_early_stop)

            loss = scores_l2 + lamda * l1S
            analyze_loss(lamda, 'l2', loss, real_anomaly, dataset, train,
                norm_s, random_seed, outer_iteration, inner_iteration,
                enable_early_stop)

            loss = scores_fm + lamda * l1S
            analyze_loss(lamda, 'fm', loss, real_anomaly, dataset, train,
                norm_s, random_seed, outer_iteration, inner_iteration,
                enable_early_stop)




def analyze_loss(lamda, method, loss, true_labels, dataset, train,
        norm_s, random_seed, outer_iteration, inner_iteration,
        enable_early_stop):

    print("\n===========Summary, lamda={}, dataset={}====".format(lamda, dataset))
    loss = np.array(loss)
    per = get_percentile(loss, dataset)
    print("percentile={}".format(per))
    y_pred = (loss >= per)*1
    mean = np.mean(loss)
    std = np.std(loss)
    print("mean={}, std={}".format(mean, std))
    # y_pred = (loss - mean > std)*1 # True=1 is anomly
    print("y_true={}".format(Counter(true_labels)))
    print("y_pred={}".format(Counter(y_pred)))

    save_results(y_pred, true_labels, norm_s, lamda, method,
        dataset, random_seed, outer_iteration, inner_iteration,
        mean, std, enable_early_stop, train)
    print("===========Summary==============\n")


def decomposeX(args):
    data = importlib.import_module("data.{}".format(args.dataset))
    X, y = data.get_mixed_train()
    print("X.shape={}".format(X.shape))
    print("y.shape={}".format(y.shape))

    #lambda_list = np.arange(0.00035, 0.0015, 0.00010)
    #lambda_list = [args.lamda_start, args.lamda_end, 0.00010)
    lambda_list = [args.lamda_start]
    #lambda_list = [0.00045, 0.00065, 0.00075, 0.00085, 0.00095]

    if args.train_and_test == 'train_and_test':
        train=True
        test=True
    elif args.train_and_test == 'train':
        train=True
        test=False
    elif args.train_and_test == 'test':
        train=False
        test=True
    else:
        train=False
        test=False

    for lam in lambda_list:
        begin_time = time.time()
        RobustGAN(X=X, y=y,
            lamda=lam,
            train=train,
            test=test,
            norm_s = args.norm_s,
            dataset=args.dataset,
            inner_iteration=args.inner,
            outer_iteration=args.outer,
            re_init=True,
            verbose=args.verbose,
            gpu_id=args.gpu_id,
            random_seed=args.random_seed,
            enable_early_stop=args.enable_early_stop)
        print("Finishing lamda={} takes {} s.".\
            format(lam, time.time() - begin_time))

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'train_and_test',
        nargs='?',
        choices=['train_and_test','train', 'test'],
        help='Choose either train or test or both')
    parser.add_argument(
        '--verbose',
        help='Verbosity with printout debug message',
        action='store_true')
    parser.add_argument(
        '--enable_early_stop',
        help='Enable early stop during training GAN',
        action='store_true')
    parser.add_argument(
        '--norm_s',
        type=str,
        help='Type of norm: either \'L1\' norm or \'L21\' of norm ||S|| or \'L21\' of transposed S^T',
        choices=['L1', 'L21', 'L21ST'],
        required=True)
    parser.add_argument(
        '--outer',
        type=int,
        help='Number of outer iterations. Default=10.',
        default=10)
    parser.add_argument(
        '--inner',
        type=int,
        help='Number of inner iterations. Default=120.',
        default=150)
    parser.add_argument(
        '--random_seed',
        type=int,
        help='Random seed number. Default=42.',
        default=42)
    parser.add_argument(
        '--lamda_start',
        type=float,
        help='Starting lamda of the list to experiment. Default is 0.00035.',
        default=0.00035)
    parser.add_argument(
        '--lamda_step',
        type=float,
        help='Increasing step of lamda of the list to experiment. Default is 0.00001.',
        default=1e-5)
    parser.add_argument(
        '--lamda_end',
        type=float,
        help='Ending lamda of the list to experiment. Default is 0.0015.',
        default=0.0015)
    parser.add_argument(
        '--gpu_id',
        type=str,
        help='GPU ID. Default=0.',
        default='0')
    parser.add_argument(
        '--dataset',
        type=str,
        help='Data set name. Default is mnist.',
        default='mnist')

    args = parser.parse_args()

    decomposeX(args)

