import tensorflow as tf
import numpy as np
import importlib
import logging
import sys
import os
import time
import random


#l_rate = 1e-5

FREQ_PRINT = 5
FREQ_EV = 1
PATIENCE = 10

IMG_DIM = 28


def batch_fill(testx, batch_size):
    """ Quick and dirty hack for filling smaller batch

    :param testx:
    :param batch_size:
    :return:
    """
    nr_batches_test = int(testx.shape[0] / batch_size)
    ran_from = nr_batches_test * batch_size
    ran_to = (nr_batches_test + 1) * batch_size
    size = testx[ran_from:ran_to].shape[0]
    new_shape = [batch_size - size]+list(testx.shape[1:])
    fill = np.ones(new_shape)
    return np.concatenate([testx[ran_from:ran_to], fill], axis=0), size


# to update neural net with moving avg variables, suitable for ss learning cf Saliman
def get_getter(ema):
    def ema_getter(getter, name, *args, **kwargs):
        var = getter(name, *args, **kwargs)
        ema_var = ema.average(var)
        return ema_var if ema_var else var
    return ema_getter

def display_parameters(batch_size, starting_lr, ema_decay, degree,
                       do_spectral_norm, random_seed, nb_epochs, enable_early_stop):
    """See parameters
    """
    print('Batch size: ', batch_size)
    print('Starting learning rate: ', starting_lr)
    print('EMA Decay: ', ema_decay)
    print('Degree for L norms: ', degree)
    print('Spectral Norm enabled: ', do_spectral_norm)
    print("Random seed: {}".format(random_seed))
    print("nb_epochs: {}".format(nb_epochs))
    print("enable_early_stop: {}".format(enable_early_stop))

def display_progression_epoch(j, id_max):
    """See epoch progression
    """
    batch_progression = int((j / id_max) * 100)
    sys.stdout.write(str(batch_progression) + ' % epoch' + chr(13))
    _ = sys.stdout.flush

class BiGAN(object):
    def __init__(self, sess, logdir, dataset,
            degree=1, enable_sm=True, enable_early_stop=False,
            input_dim_list=[784]):
        """
        Args:
            input_dim_list: must include the original data dimension
            degree (int): degree of the norm in the feature matching
            enable_sm (bool): allow TF summaries for monitoring the training
        """
        # Parameters
        self.ema_decay = 0.999
        self.degree=degree
        self.enable_sm=enable_sm
        self.logdir=logdir
        self.enable_early_stop = enable_early_stop
        self.dataset = dataset
        model = importlib.import_module('model.BasicBiGAN.{}_utilities'.
            format(dataset)) #mnist_utilities')
        self.batch_size = model.batch_size
        self.starting_lr = model.learning_rate
        self.latent_dim = model.latent_dim

        self.global_step = tf.Variable(0, name='global_step', trainable=False)

        # Placeholders
        #input_shape= (self.batch_size, IMG_DIM, IMG_DIM, 1)
        data = importlib.import_module("data.{}".format(dataset))
        self.input_shape = data.get_shape_input()
        self.x_pl = tf.compat.v1.placeholder(tf.float32,
            shape=self.input_shape, name="input_x")
        print("self.x_pl.shape={}".format(self.x_pl.get_shape()))
        self.z_pl = tf.compat.v1.placeholder(tf.float32, shape=[None, self.latent_dim],
                              name="input_z")
        self.is_training_pl = tf.compat.v1.placeholder(tf.bool, [], name='is_training_pl')
        self.learning_rate = tf.compat.v1.placeholder(tf.float32, shape=(), name="lr_pl")
        self.gen = model.decoder
        self.enc = model.encoder
        self.dis_xz = model.discriminator_xz
        self.dis_xx = model.discriminator_xx
        self.dis_zz = model.discriminator_zz

        logging.basicConfig()
        self.logger = logging.getLogger("BiGAN.run")
        self.logger.warn('Building graph ALICE BiGAN...')
        do_spectral_norm = True

        with tf.compat.v1.variable_scope('encoder_model'):
            self.z_gen = self.enc(self.x_pl, is_training=self.is_training_pl,
                        do_spectral_norm=do_spectral_norm)

        with tf.compat.v1.variable_scope('generator_model'):
            self.x_gen = self.gen(self.z_pl, is_training=self.is_training_pl)
            self.rec_x = self.gen(self.z_gen, is_training=self.is_training_pl,
                reuse=True)

        with tf.compat.v1.variable_scope('encoder_model'):
            self.rec_z = self.enc(self.x_gen, is_training=self.is_training_pl,
                reuse=True, do_spectral_norm=do_spectral_norm)

        with tf.compat.v1.variable_scope('discriminator_model_xz'):
            l_encoder, self.inter_layer_inp_xz = self.dis_xz(self.x_pl, self.z_gen,
                        is_training=self.is_training_pl,
                        do_spectral_norm=do_spectral_norm)
            self.l_generator, inter_layer_rct_xz = self.dis_xz(self.x_gen, self.z_pl,
                        is_training=self.is_training_pl,
                        reuse=True,
                        do_spectral_norm=do_spectral_norm)

        with tf.compat.v1.variable_scope('discriminator_model_xx'):
            x_logit_real, inter_layer_inp_xx = self.dis_xx(self.x_pl, self.x_pl,
                        is_training=self.is_training_pl,
                        do_spectral_norm=do_spectral_norm)
            x_logit_fake, inter_layer_rct_xx = self.dis_xx(self.x_pl, self.rec_x,
                        is_training=self.is_training_pl,
                        reuse=True, do_spectral_norm=do_spectral_norm)

        with tf.compat.v1.variable_scope('discriminator_model_zz'):
            z_logit_real, _ = self.dis_zz(self.z_pl, self.z_pl, is_training=self.is_training_pl,
                                     do_spectral_norm=do_spectral_norm)
            z_logit_fake, _ = self.dis_zz(self.z_pl, self.rec_z, is_training=self.is_training_pl,
                                  reuse=True, do_spectral_norm=do_spectral_norm)

        with tf.name_scope('loss_functions'):
            # discriminator xz
            loss_dis_enc = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                labels=tf.ones_like(l_encoder), logits=l_encoder))
            loss_dis_gen = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                labels=tf.zeros_like(self.l_generator), logits=self.l_generator))
            self.dis_loss_xz = loss_dis_gen + loss_dis_enc

            # discriminator xx
            x_real_dis = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=x_logit_real, labels=tf.ones_like(x_logit_real))
            x_fake_dis = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=x_logit_fake, labels=tf.zeros_like(x_logit_fake))
            self.dis_loss_xx = tf.reduce_mean(x_real_dis + x_fake_dis)

            # discriminator zz
            z_real_dis = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=z_logit_real, labels=tf.ones_like(z_logit_real))
            z_fake_dis = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=z_logit_fake, labels=tf.zeros_like(z_logit_fake))
            self.dis_loss_zz = tf.reduce_mean(z_real_dis + z_fake_dis)

            self.loss_discriminator = self.dis_loss_xz + self.dis_loss_xx +\
                self.dis_loss_zz

            # generator and encoder
            gen_loss_xz = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                labels=tf.ones_like(self.l_generator),logits=self.l_generator))
            enc_loss_xz = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
                labels=tf.zeros_like(l_encoder), logits=l_encoder))
            x_real_gen = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=x_logit_real, labels=tf.zeros_like(x_logit_real))
            x_fake_gen = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=x_logit_fake, labels=tf.ones_like(x_logit_fake))
            z_real_gen = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=z_logit_real, labels=tf.zeros_like(z_logit_real))
            z_fake_gen = tf.nn.sigmoid_cross_entropy_with_logits(
                logits=z_logit_fake, labels=tf.ones_like(z_logit_fake))

            cost_x = tf.reduce_mean(x_real_gen + x_fake_gen)
            cost_z = tf.reduce_mean(z_real_gen + z_fake_gen)

            cycle_consistency_loss = cost_x + cost_z
            self.loss_generator = gen_loss_xz + cycle_consistency_loss
            self.loss_encoder = enc_loss_xz + cycle_consistency_loss

        with tf.name_scope('optimizers'):
            # control op dependencies for batch norm and trainable variables
            tvars = tf.compat.v1.trainable_variables()
            dxzvars = [var for var in tvars if 'discriminator_model_xz' in var.name]
            dxxvars = [var for var in tvars if 'discriminator_model_xx' in var.name]
            dzzvars = [var for var in tvars if 'discriminator_model_zz' in var.name]
            gvars = [var for var in tvars if 'generator_model' in var.name]
            evars = [var for var in tvars if 'encoder_model' in var.name]

            update_ops = tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.UPDATE_OPS)
            update_ops_gen = [x for x in update_ops if ('generator_model' in x.name)]
            update_ops_enc = [x for x in update_ops if ('encoder_model' in x.name)]
            update_ops_dis_xz = [x for x in update_ops if
                                 ('discriminator_model_xz' in x.name)]
            update_ops_dis_xx = [x for x in update_ops if
                                 ('discriminator_model_xx' in x.name)]
            update_ops_dis_zz = [x for x in update_ops if
                                 ('discriminator_model_zz' in x.name)]

            optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=self.learning_rate,
                                                      beta1=0.5)

            with tf.control_dependencies(update_ops_gen):
                gen_op = optimizer.minimize(self.loss_generator, var_list=gvars,
                                                global_step=self.global_step)
            with tf.control_dependencies(update_ops_enc):
                enc_op = optimizer.minimize(self.loss_encoder, var_list=evars)

            with tf.control_dependencies(update_ops_dis_xz):
                dis_op_xz = optimizer.minimize(self.dis_loss_xz, var_list=dxzvars)

            with tf.control_dependencies(update_ops_dis_xx):
                dis_op_xx = optimizer.minimize(self.dis_loss_xx, var_list=dxxvars)

            with tf.control_dependencies(update_ops_dis_zz):
                dis_op_zz = optimizer.minimize(self.dis_loss_zz, var_list=dzzvars)

            # Exponential Moving Average for inference
            def train_op_with_ema_dependency(vars, op):
                ema = tf.train.ExponentialMovingAverage(decay=self.ema_decay)
                maintain_averages_op = ema.apply(vars)
                with tf.control_dependencies([op]):
                    train_op = tf.group(maintain_averages_op)
                return train_op, ema

            self.train_gen_op, gen_ema = train_op_with_ema_dependency(gvars, gen_op)
            self.train_enc_op, enc_ema = train_op_with_ema_dependency(evars, enc_op)
            self.train_dis_op_xz, xz_ema = train_op_with_ema_dependency(dxzvars,
                                                                   dis_op_xz)
            self.train_dis_op_xx, xx_ema = train_op_with_ema_dependency(dxxvars,
                                                                   dis_op_xx)
            self.train_dis_op_zz, zz_ema = train_op_with_ema_dependency(dzzvars,
                                                                   dis_op_zz)

        with tf.compat.v1.variable_scope('encoder_model'):
            self.z_gen_ema = self.enc(self.x_pl, is_training=self.is_training_pl,
                            getter=get_getter(enc_ema), reuse=True,
                            do_spectral_norm=do_spectral_norm)

        with tf.compat.v1.variable_scope('generator_model'):
            self.rec_x_ema = self.gen(self.z_gen_ema, is_training=self.is_training_pl,
                                  getter=get_getter(gen_ema), reuse=True)
            x_gen_ema = self.gen(self.z_pl, is_training=self.is_training_pl,
                                  getter=get_getter(gen_ema), reuse=True)

        with tf.compat.v1.variable_scope('discriminator_model_xx'):
            l_encoder_emaxx, inter_layer_inp_emaxx = self.dis_xx(self.x_pl, self.x_pl,
                        is_training=self.is_training_pl,
                        getter=get_getter(xx_ema),
                        reuse=True,
                        do_spectral_norm=do_spectral_norm)

            l_generator_emaxx, inter_layer_rct_emaxx = self.dis_xx(self.x_pl, self.rec_x_ema,
                        is_training=self.is_training_pl,
                        getter=get_getter(xx_ema),
                        reuse=True,
                        do_spectral_norm=do_spectral_norm)

        with tf.name_scope('Testing'):
            with tf.compat.v1.variable_scope('Scores'):
                self.score_ch = tf.nn.sigmoid_cross_entropy_with_logits(
                        labels=tf.ones_like(l_generator_emaxx),
                        logits=l_generator_emaxx)
                self.score_ch = tf.squeeze(self.score_ch)

                rec = self.x_pl - self.rec_x_ema
                rec = tf.contrib.layers.flatten(rec)
                self.score_l1 = tf.norm(rec, ord=1, axis=1,
                                keepdims=False, name='d_loss')
                self.score_l1 = tf.squeeze(self.score_l1)

                rec = self.x_pl - self.rec_x_ema
                rec = tf.contrib.layers.flatten(rec)
                self.score_l2 = tf.norm(rec, ord=2, axis=1,
                                keepdims=False, name='d_loss')
                self.score_l2 = tf.squeeze(self.score_l2)

                inter_layer_inp, inter_layer_rct = inter_layer_inp_emaxx, \
                                                   inter_layer_rct_emaxx
                fm = inter_layer_inp - inter_layer_rct
                fm = tf.contrib.layers.flatten(fm)
                self.score_fm = tf.norm(fm, ord=self.degree, axis=1,
                                 keepdims=False, name='d_loss')
                self.score_fm = tf.squeeze(self.score_fm)

        if self.enable_early_stop:
            self.rec_error_valid = tf.reduce_mean(self.score_fm)

        if self.enable_sm:
            with tf.name_scope('summary'):
                with tf.name_scope('dis_summary'):
                    tf.compat.v1.summary.scalar('loss_discriminator', self.loss_discriminator, ['dis'])
                    tf.compat.v1.summary.scalar('loss_dis_encoder', loss_dis_enc, ['dis'])
                    tf.compat.v1.summary.scalar('loss_dis_gen', loss_dis_gen, ['dis'])
                    tf.compat.v1.summary.scalar('loss_dis_xz', self.dis_loss_xz, ['dis'])
                    tf.compat.v1.summary.scalar('loss_dis_xx', self.dis_loss_xx, ['dis'])
                    tf.compat.v1.summary.scalar('loss_dis_zz', self.dis_loss_zz, ['dis'])

                with tf.name_scope('gen_summary'):
                    tf.compat.v1.summary.scalar('loss_generator', self.loss_generator, ['gen'])
                    tf.compat.v1.summary.scalar('loss_encoder', self.loss_encoder, ['gen'])
                    tf.compat.v1.summary.scalar('loss_encgen_dxx', cost_x, ['gen'])
                    tf.compat.v1.summary.scalar('loss_encgen_dzz', cost_z, ['gen'])

                if self.enable_early_stop:
                    with tf.name_scope('validation_summary'):
                       tf.summary.scalar('valid', self.rec_error_valid, ['v'])

                with tf.name_scope('img_summary'):
                    heatmap_pl_latent = tf.compat.v1.placeholder(tf.float32,
                                                       shape=(1, 480, 640, 3),
                                                       name="heatmap_pl_latent")
                    sum_op_latent = tf.compat.v1.summary.image('heatmap_latent', heatmap_pl_latent)

                sum_op_dis = tf.compat.v1.summary.merge_all('dis')
                sum_op_gen = tf.compat.v1.summary.merge_all('gen')
                self.sum_op = tf.compat.v1.summary.merge([sum_op_dis, sum_op_gen])
                self.sum_op_valid = tf.compat.v1.summary.merge_all('v')

        self.saver = tf.compat.v1.train.Saver(max_to_keep=2, save_relative_paths=True)
        print("OK construction BiGAN....")

    def restore(self, sess):
        # restore the model for continue training or inference
        save_path = "{}/".format(self.logdir)
        try:
            self.saver.restore(sess, tf.train.latest_checkpoint(save_path))
            print("At {} Restore model from {}".
                format(os.getcwd(), save_path))
            return 0
        except ValueError:
            return -1


    def fit(self, sess, X, nb_epochs, random_seed=42, verbose=False):
        """ Runs the AliCE on the specified dataset

        Note:
            Saves summaries on tensorboard. To display them, please use cmd line
            tensorboard --logdir=model.training_logdir() --port=number
        Args:
            dataset (str): name of the dataset
            nb_epochs (int): number of epochs
            random_seed (int): trying different seeds for averaging the results
        """
        if self.restore(sess) != 0:
            sess.run(tf.global_variables_initializer())

        if self.dataset == 'mnist':
            X = tf.reshape(X, [-1, IMG_DIM, IMG_DIM, 1]).eval()

        if verbose:
            display_parameters(self.batch_size, self.starting_lr, self.ema_decay,
                self.degree, True, random_seed, nb_epochs, self.enable_early_stop)

        # Data
        trainx = X
        trainx_copy = trainx.copy()

        if verbose:
            self.logger.setLevel(logging.INFO)

        step = sess.run(self.global_step)
        self.logger.info('Start training...step={}'.format(step))
        writer = tf.compat.v1.summary.FileWriter(self.logdir, sess.graph)
        train_batch = 0
        epoch = 0
        best_valid_loss = 0

        while epoch < nb_epochs:
            lr = self.starting_lr
            begin = time.time()

            rng = np.random.RandomState(random_seed)
            if self.enable_early_stop:
                """Split train and valid: 90:10 ratio
                and change the index every epoch
                """
                valid_size = int(0.1 * len(X))
                valid_ids = random.sample(range(len(X)), valid_size)
                mask_valid = np.ones(len(X), dtype=bool)
                mask_valid[valid_ids] = False
                validx = X[valid_ids]
                trainx = X[mask_valid]
                trainx_copy = trainx.copy()
                #print("trainx.shape={}".format(trainx.shape))
                #print("validx.shape={}".format(validx.shape))

             # construct randomly permuted minibatches
            trainx = trainx[rng.permutation(trainx.shape[0])]  # shuffling dataset
            trainx_copy = trainx_copy[rng.permutation(trainx.shape[0])]
            train_loss_dis_xz, train_loss_dis_xx,  train_loss_dis_zz, \
            train_loss_dis, train_loss_gen, train_loss_enc = [0, 0, 0, 0, 0, 0]
            nr_batches_train = int(trainx.shape[0] / self.batch_size)

            # Training
            for t in range(nr_batches_train):
                display_progression_epoch(t, nr_batches_train)
                ran_from = t * self.batch_size
                ran_to = (t + 1) * self.batch_size

                # train discriminator
                feed_dict = {self.x_pl: trainx[ran_from:ran_to],
                             self.z_pl: np.random.normal(size=[self.batch_size, self.latent_dim]),
                             self.is_training_pl: True,
                             self.learning_rate:lr}

                _, _, _, ld, ldxz, ldxx, ldzz, step = sess.run([
                    self.train_dis_op_xz, self.train_dis_op_xx, self.train_dis_op_zz,
                    self.loss_discriminator, self.dis_loss_xz, self.dis_loss_xx,
                    self.dis_loss_zz, self.global_step],
                    feed_dict=feed_dict)
                train_loss_dis += ld
                train_loss_dis_xz += ldxz
                train_loss_dis_xx += ldxx
                train_loss_dis_zz += ldzz

                # train generator and encoder
                feed_dict = {self.x_pl: trainx_copy[ran_from:ran_to],
                             self.z_pl: np.random.normal(size=[self.batch_size, self.latent_dim]),
                             self.is_training_pl: True,
                             self.learning_rate:lr}
                _,_, le, lg = sess.run([self.train_gen_op, self.train_enc_op,
                    self.loss_encoder, self.loss_generator], feed_dict=feed_dict)
                train_loss_gen += lg
                train_loss_enc += le

                if (train_loss_gen + train_loss_enc) < train_loss_dis:
                    _, _, _, ld, ldxz, ldxx, ldzz, step = sess.run([
                        self.train_dis_op_xz, self.train_dis_op_xx, self.train_dis_op_zz,
                        self.loss_discriminator, self.dis_loss_xz, self.dis_loss_xx,
                        self.dis_loss_zz, self.global_step],
                        feed_dict=feed_dict)

                if train_loss_dis * 2 < (train_loss_gen + train_loss_enc):
                    _,_, le, lg = sess.run([self.train_gen_op, self.train_enc_op,
                        self.loss_encoder, self.loss_generator], feed_dict=feed_dict)

                if self.enable_sm:
                    sm = sess.run(self.sum_op, feed_dict=feed_dict)
                    writer.add_summary(sm, step)
                train_batch += 1

            train_loss_gen /= nr_batches_train
            train_loss_enc /= nr_batches_train
            train_loss_dis /= nr_batches_train
            train_loss_dis_xz /= nr_batches_train
            train_loss_dis_xx /= nr_batches_train
            train_loss_dis_zz /= nr_batches_train

            self.logger.info('Epoch terminated')
            if epoch % FREQ_PRINT == 0:
                print("Epoch %d | time = %ds | loss gen = %.4f | loss enc = %.4f | "
                    "loss dis = %.4f | loss dis xz = %.4f | loss dis xx = %.4f | "
                    "loss dis zz = %.4f"
                    % (epoch, time.time() - begin, train_loss_gen,
                        train_loss_enc, train_loss_dis, train_loss_dis_xz,
                        train_loss_dis_xx, train_loss_dis_zz))

            ##EARLY STOPPING
            if (epoch + 1) % FREQ_EV == 0 and self.enable_early_stop:

                valid_loss = 0
                nr_batches_valid = int(validx.shape[0] / self.batch_size)
                begin_valid_time = time.time()
                for t in range(nr_batches_valid):
                    # construct randomly permuted minibatches
                    ran_from = t * self.batch_size
                    ran_to = (t + 1) * self.batch_size
                    feed_dict = {self.x_pl: validx[ran_from:ran_to],
                             self.z_pl: np.random.normal(size=[validx.shape[0], self.latent_dim]),
                             self.is_training_pl: False}
                    vl, lat = sess.run([self.rec_error_valid, self.rec_z], feed_dict=feed_dict)
                    if self.enable_sm:
                        sm = sess.run(self.sum_op_valid, feed_dict=feed_dict)
                        writer.add_summary(sm, step)  # train_batch)
                    valid_loss += vl

                if validx.shape[0] % self.batch_size != 0:
                    batch, size = batch_fill(validx, self.batch_size)
                    feed_dict = {self.x_pl: batch,
                             self.z_pl: np.random.normal(size=[validx.shape[0], self.latent_dim]),
                             self.is_training_pl: False}
                    vl, lat = sess.run([self.rec_error_valid, self.rec_z], feed_dict=feed_dict)
                    if self.enable_sm:
                        sm = sess.run(self.sum_op_valid, feed_dict=feed_dict)
                        writer.add_summary(sm, step)  # train_batch)
                    valid_loss += vl

                valid_loss /= nr_batches_valid

                self.logger.info('Validation takes {}s: valid loss {:.4f}'.
                    format(time.time() - begin_valid_time, valid_loss))

                if (valid_loss < best_valid_loss or epoch == FREQ_EV-1):
                    best_valid_loss = valid_loss
                    self.logger.warn("Best model epoch={} - valid loss = {:.4f} - saving...".format(epoch, best_valid_loss))
                    self.saver.save(sess, self.logdir+'/model.ckpt', global_step=step)
                    nb_without_improvements = 0
                    print("Epoch %d | time = %ds | loss gen = %.4f | loss enc = %.4f | "
                        "loss dis = %.4f | loss dis xz = %.4f | loss dis xx = %.4f | "
                        "loss dis zz = %.4f"
                        % (epoch, time.time() - begin, train_loss_gen,
                            train_loss_enc, train_loss_dis, train_loss_dis_xz,
                            train_loss_dis_xx, train_loss_dis_zz))
                else:
                    nb_without_improvements += FREQ_EV

                if nb_without_improvements > PATIENCE:
                    self.logger.warning(
                      "Early stopping at epoch {} with weights from epoch {}".format(
                          epoch, epoch - nb_without_improvements))
                    epoch = nb_epochs # force to stop
                    break

            epoch += 1

        self.saver.save(sess, self.logdir+'/model.ckpt', global_step=step)

    def evaluate(self, sess, testx):
        step = sess.run(self.global_step)
        self.logger.warn('Testing evaluation...step={}'.format(step))
        if self.dataset == 'mnist':
            testx = tf.reshape(testx, [-1, IMG_DIM, IMG_DIM, 1]).eval()
        nr_batches_test = int(testx.shape[0] / self.batch_size)

        scores_ch = []
        scores_l1 = []
        scores_l2 = []
        scores_fm = []
        inference_time = []

        # Create scores
        for t in range(nr_batches_test):
            # construct randomly permuted minibatches
            ran_from = t * self.batch_size
            ran_to = (t + 1) * self.batch_size
            begin_test_time_batch = time.time()

            feed_dict = {self.x_pl: testx[ran_from:ran_to],
                         self.z_pl: np.random.normal(size=[self.batch_size, self.latent_dim]),
                         self.is_training_pl:False}

            scores_ch += sess.run(self.score_ch, feed_dict=feed_dict).tolist()
            scores_l1 += sess.run(self.score_l1, feed_dict=feed_dict).tolist()
            scores_l2 += sess.run(self.score_l2, feed_dict=feed_dict).tolist()
            scores_fm += sess.run(self.score_fm, feed_dict=feed_dict).tolist()
            inference_time.append(time.time() - begin_test_time_batch)

        inference_time = np.mean(inference_time)
        self.logger.info('Testing : mean inference time is %.4f' % (inference_time))

        if testx.shape[0] % self.batch_size != 0:

            batch, size = batch_fill(testx, self.batch_size)
            feed_dict = {self.x_pl: batch,
                         self.z_pl: np.random.normal(size=[self.batch_size, self.latent_dim]),
                         self.is_training_pl: False}

            bscores_ch = sess.run(self.score_ch,feed_dict=feed_dict).tolist()
            bscores_l1 = sess.run(self.score_l1,feed_dict=feed_dict).tolist()
            bscores_l2 = sess.run(self.score_l2,feed_dict=feed_dict).tolist()
            bscores_fm = sess.run(self.score_fm,feed_dict=feed_dict).tolist()

            scores_ch += bscores_ch[:size]
            scores_l1 += bscores_l1[:size]
            scores_l2 += bscores_l2[:size]
            scores_fm += bscores_fm[:size]

        return scores_ch, scores_l1, scores_l2, scores_fm

    def transform(self, sess, X):
        no_batch = int(X.shape[0] / self.batch_size)
        hiddens_z = np.array([])
        for i in range(no_batch):
            i_from = i * self.batch_size
            i_to   = (i+1) * self.batch_size
            Xb = X[i_from:i_to]
            if self.dataset == 'mnist':
                Xb = tf.reshape(Xb, [-1, IMG_DIM, IMG_DIM, 1]).eval()
            feed_dict={self.x_pl:Xb, self.is_training_pl:False}
            z = self.z_gen_ema.eval(session=sess,feed_dict=feed_dict)
            hiddens_z = np.vstack([hiddens_z, z]) if hiddens_z.size else z

        if X.shape[0] % self.batch_size != 0:
            batch, size = batch_fill(X, self.batch_size)
            if self.dataset == 'mnist':
                batch = tf.reshape(batch, [-1, IMG_DIM, IMG_DIM, 1]).eval()
            feed_dict={self.x_pl:batch, self.is_training_pl:False}
            z = self.z_gen_ema.eval(session=sess,feed_dict=feed_dict)
            z = z[:size]
            hiddens_z = np.vstack([hiddens_z, z]) if hiddens_z.size else z

        #print("hiddens_z.shape={}".format(hiddens_z.shape))
        return hiddens_z


    def getRecon(self, sess, X):
        no_batch = int(X.shape[0] / self.batch_size)
        recon_x = None
        recon_x = np.array([])
        for i in range(no_batch):
            i_from = i * self.batch_size
            i_to   = (i+1) * self.batch_size
            Xb = X[i_from:i_to]
            if self.dataset == 'mnist':
                Xb = tf.reshape(Xb, [-1, IMG_DIM, IMG_DIM, 1]).eval()
            feed_dict={self.x_pl:Xb, self.is_training_pl:False}
            recx = self.rec_x_ema.eval(session=sess, feed_dict=feed_dict)
            if self.dataset == 'mnist':
                recx = tf.reshape(recx, [-1, IMG_DIM * IMG_DIM]).eval()
            recon_x = np.vstack([recon_x, recx]) if recon_x.size else recx

        if X.shape[0] % self.batch_size != 0:
            batch, size = batch_fill(X, self.batch_size)
            if self.dataset == 'mnist':
                batch = tf.reshape(batch, [-1, IMG_DIM, IMG_DIM, 1]).eval()
            feed_dict={self.x_pl:batch, self.is_training_pl:False}
            recx = self.rec_x_ema.eval(session=sess, feed_dict=feed_dict)
            recx = recx[:size]
            if self.dataset == 'mnist':
                recx = tf.reshape(recx, [-1, IMG_DIM * IMG_DIM]).eval()
            recon_x = np.vstack([recon_x, recx]) if recon_x.size else recx

        #print("resized recon_x.shape={}".format(np.shape(recon_x)))
        return recon_x

