import tensorflow as tf
from tensorflow import keras
import numpy as np
import importlib
import logging
import sys
import os
import time
from utils import spectral_norm

batch_size = 32
learning_rate = 0.0001
latent_dim = 100
IMG_DIM = 28

init_kernel = tf.contrib.layers.xavier_initializer()

def leakyReLu(x, alpha=0.2, name=None):
    #return tf.maximum(x, tf.multiply(x, 0.2))
    if name:
        with tf.compat.v1.variable_scope(name):
            return tf.nn.relu(x) - (alpha * tf.nn.relu(-x))
    else:
        return tf.nn.relu(x) - (alpha * tf.nn.relu(-x))

def encoder(x_inp, is_training=False, getter=None, reuse=False,
            do_spectral_norm=True):
    """ Encoder: maps the data into the latent space

    Agrs:
        x (tensor): input data for the encoder.
        is_training (bool): for batch norms and dropouts

    Returns:
        net (tensor): last activation layer of the encoder
    """
    layers = spectral_norm if do_spectral_norm else tf.layers

    with tf.compat.v1.variable_scope('encoder', reuse=reuse, custom_getter=getter):
        x = tf.reshape(x_inp, [-1, IMG_DIM, IMG_DIM, 1])
        name_net = 'layer_1'
        with tf.compat.v1.variable_scope(name_net):
            net = layers.conv2d(x,
                                128,
                                kernel_size=4,
                                padding='SAME',
                                strides=2,
                                kernel_initializer=init_kernel,
                                name='conv')
            net = tf.layers.batch_normalization(net, training=is_training)
            net = leakyReLu(net, name='leaky_relu')

        name_net = 'layer_2'
        with tf.compat.v1.variable_scope(name_net):
            net = layers.conv2d(net,
                           256,
                           kernel_size=4,
                           padding='SAME',
                           strides=2,
                           kernel_initializer=init_kernel,
                           name='conv')
            net = tf.layers.batch_normalization(net,
                                                training=is_training)
            net = leakyReLu(net, name='leaky_relu')

        name_net = 'layer_3'
        with tf.compat.v1.variable_scope(name_net):
            net = layers.conv2d(net,
                           512,
                           kernel_size=4,
                           padding='SAME',
                           strides=2,
                           kernel_initializer=init_kernel,
                           name='conv')
            net = tf.layers.batch_normalization(net,
                                                training=is_training)
            net = leakyReLu(net, name='leaky_relu')

        name_net = 'layer_4'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.compat.v1.layers.conv2d(net,
                                   latent_dim,
                                   kernel_size=4,
                                   strides=1,
                                   padding='VALID',
                                   kernel_initializer=init_kernel,
                                   name='conv')
            net = tf.squeeze(net, [1, 2])
            #print("encoder net.shape={}".format(net.get_shape()))

    return net

def decoder(z, is_training=False, getter=None, reuse=False):
    """ Decoder (Generator): generates data from the latent space

    Agrs:
        z (tensor): input data for the encoder.
        is_training (bool): for batch norms and dropouts

    Returns:
        net (tensor): last activation layer of the generator
        shape=28 x 28
    """

    with tf.compat.v1.variable_scope('generator', reuse=reuse, custom_getter=getter):
        net = tf.reshape(z, [-1, 1, 1, latent_dim])
        name_net = 'layer_1'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.compat.v1.layers.dense(net,
                                    units = 6 * 6 * 32,
                                    activation=leakyReLu)
            net = tf.layers.batch_normalization(net,
                                    training=is_training,
                                    name='tdense/batch_normalization')
            #net = tf.nn.tanh(net, name='tdense/tanh')
            net = tf.reshape(net, shape=[-1, 6, 6, 32])
            #print("decoder1 net.shape={}".format(net.get_shape()))

        name_net = 'layer_2'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.compat.v1.layers.conv2d_transpose(net,
                                     filters=512,
                                     kernel_size=[4,4],
                                     strides=(2,2),
                                     padding='VALID',
                                     kernel_initializer=init_kernel,
                                     name='tconv2')

            net = tf.layers.batch_normalization(net,
                                     training=is_training,
                                     name='tconv2/batch_normalization')

            net = tf.nn.relu(net, name='tconv2/relu')

        name_net = 'layer_3'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.layers.conv2d_transpose(net,
                                     filters=256,
                                     kernel_size=[4,4],
                                     strides=(1,1),
                                     padding='SAME',
                                     kernel_initializer=init_kernel,
                                     name='tconv3')

            net = tf.layers.batch_normalization(net,
                                     training=is_training,
                                     name='tconv3/batch_normalization')

            net = tf.nn.relu(net, name='tconv3/relu')
            #print("decoder2 net.shape={}".format(net.get_shape()))

        name_net = 'layer_4'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.layers.conv2d_transpose(net,
                                     filters=128,
                                     kernel_size=2,
                                     strides=1,
                                     padding='SAME',
                                     kernel_initializer=init_kernel,
                                     name='tconv4')

            net = tf.layers.batch_normalization(net,
                                     training=is_training,
                                     name='tconv4/batch_normalization')
            net = tf.nn.relu(net, name='tconv4/relu')
            #print("decoder3 net.shape={}".format(net.get_shape()))

        name_net = 'layer_5'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.layers.conv2d_transpose(net,
                                     filters=1,
                                     kernel_size=2,
                                     strides=2,
                                     padding='SAME',
                                     kernel_initializer=init_kernel,
                                     name='tconv5')

            net = tf.tanh(net, name='tconv5/tanh')
            #net = tf.reshape(net, [-1, IMG_DIM * IMG_DIM])
            #print("decoder4 net.shape={}".format(net.get_shape()))

    return net

def discriminator_xz(x_inp, z_inp, is_training=False, getter=None, reuse=False,
            do_spectral_norm=False):
    """
    Discriminates between pairs (E(x), x) and (z, G(z))
    Args:
        x_inp (tensor): input data for the discriminator.
        z_inp (tensor): input variable in the latent space.
        is_training (bool): for batch norms and dropouts
        do_spectral_norm (bool): apply spectral normalization for discriminator

    Returns:
        logits (tensor): last activation layer of the discriminator (shape 1)
        intermediate_layer (tensor): intermediate layer for feature matching
    """
    layers = spectral_norm if do_spectral_norm else tf.layers

    with tf.compat.v1.variable_scope('discriminator_xz', reuse=reuse,
            custom_getter=getter):
        # D(x)
        name_x = 'x_layer_1'
        with tf.compat.v1.variable_scope(name_x):
            x_inp = tf.reshape(x_inp, [-1, IMG_DIM, IMG_DIM, 1])

            x = layers.conv2d(x_inp,
                                 filters=128,
                                 kernel_size=4,
                                 strides=2,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv1')

            x = leakyReLu(x, 0.2, name='conv1/leaky_relu')

        name_net = 'x_layer_2'
        with tf.compat.v1.variable_scope(name_net):
            x = layers.conv2d(x,
                                 filters=256,
                                 kernel_size=4,
                                 strides=2,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv2')

            x = tf.layers.batch_normalization(x,
                                 training=is_training,
                                 name='conv2/batch_normalization')

            x = leakyReLu(x, 0.2, name='conv2/leaky_relu')

        name_net = 'x_layer_3'
        with tf.compat.v1.variable_scope(name_net):
            x = layers.conv2d(x,
                                 filters=512,
                                 kernel_size=4,
                                 strides=2,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv3')

            x = tf.layers.batch_normalization(x,
                                 training=is_training,
                                 name='conv3/batch_normalization')

            x = leakyReLu(x, 0.2, name='conv3/leaky_relu')

        x = tf.reshape(x, [-1,1,1,512*4*4])

        # D(z)
        z = tf.reshape(z_inp, [-1, 1, 1, latent_dim])

        name_z = 'z_layer_1'
        with tf.compat.v1.variable_scope(name_z):
            z = layers.conv2d(z,
                                 filters=512,
                                 kernel_size=1,
                                 strides=1,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv')
            z = leakyReLu(z)
            z = tf.layers.dropout(z, rate=0.2, training=is_training,
                                  name='dropout')

        name_net = 'z_layer_2'
        with tf.compat.v1.variable_scope(name_net):
            z = layers.conv2d(z,
                                 filters=512,
                                 kernel_size=1,
                                 strides=1,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv')
            z = leakyReLu(z)
            z = tf.layers.dropout(z, rate=0.2, training=is_training,
                                  name='dropout')

        # D(x,z)
        #print("x size={}, z size {}".
        #    format(x.get_shape(), z.get_shape()))
        y = tf.concat([x, z], axis=-1)
        #print("y.shape={}".format(y.get_shape()))

        name_y = 'y_layer_1'
        with tf.compat.v1.variable_scope(name_y):
            y = layers.conv2d(y,
                                 filters=1024,
                                 kernel_size=1,
                                 strides=1,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv')
            y = leakyReLu(y)
            y = tf.layers.dropout(y, rate=0.2, training=is_training,
                                  name='dropout')

        intermediate_layer = y

        name_net = 'y_layer_2'
        with tf.compat.v1.variable_scope(name_net):
            y = tf.layers.conv2d(y,
                                 filters=1,
                                 kernel_size=1,
                                 strides=1,
                                 padding='SAME',
                                 kernel_initializer=init_kernel,
                                 name='conv')
            #y = leakyReLu(y)

        logits = tf.squeeze(y)

    return logits, intermediate_layer

def discriminator_xx(x, rec_x, is_training=False,getter=None, reuse=False,
            do_spectral_norm=False):
    """ Discriminator architecture in tensorflow

    Discriminates between (x,x) and (x,rec_x)

    Args:
        x (tensor): input from the data space
        rec_x (tensor): reconstructed data
        is_training (bool): for batch norms and dropouts
        getter: for exponential moving average during inference
        reuse (bool): sharing variables or not

    Returns:
        logits (tensor): last activation layer of the discriminator
        intermediate_layer (tensor): intermediate layer for feature matching

    """
    layers = spectral_norm if do_spectral_norm else tf.layers

    with tf.compat.v1.variable_scope('discriminator_xx', reuse=reuse,
            custom_getter=getter):
        x = tf.reshape(x, [-1, IMG_DIM, IMG_DIM, 1])
        rec_x = tf.reshape(rec_x, [-1, IMG_DIM, IMG_DIM, 1])
        #print("x.shape={}, rec_x.shape={}".format(x.get_shape(), rec_x.get_shape()))
        net = tf.concat([x, rec_x], axis=1)
        #print("x_recx.shape={}".format(net.get_shape()))

        name_net = 'layer_1'
        with tf.compat.v1.variable_scope(name_net):
            net = layers.conv2d(net,
                           filters=64,
                           kernel_size=5,
                           strides=2,
                           padding='SAME',
                           kernel_initializer=init_kernel,
                           name='conv1')

            net = leakyReLu(net, 0.2, name='conv1/leaky_relu')

            net = tf.layers.dropout(net, rate=0.2, training=is_training,
                                  name='dropout')

        name_net = 'layer_2'
        with tf.compat.v1.variable_scope(name_net):
            net = layers.conv2d(net,
                           filters=128,
                           kernel_size=5,
                           strides=2,
                           padding='SAME',
                           kernel_initializer=init_kernel,
                           name='conv2')

            net = leakyReLu(net, 0.2, name='conv2/leaky_relu')

            net = tf.layers.dropout(net, rate=0.2, training=is_training,
                                  name='dropout')

        net = tf.contrib.layers.flatten(net)

        intermediate_layer = net
        name_net = 'layer_3'
        with tf.compat.v1.variable_scope(name_net):
            net = tf.layers.dense(net,
                                  units=1,
                                  kernel_initializer=init_kernel,
                                  name='fc')

            logits = tf.squeeze(net)

    return logits, intermediate_layer

def discriminator_zz(z, rec_z, is_training=False, getter=None, reuse=False,
        do_spectral_norm=False):
    """ Discriminator architecture in tensorflow

    Discriminates between (z,z) and (z,rec_z)

    Args:
        z (tensor): input from the latent space
        rec_z (tensor): reconstructed data
        is_training (bool): for batch norms and dropouts
        getter: for exponential moving average during inference
        reuse (bool): sharing variables or not

    Returns:
        logits (tensor): last activation layer of the discriminator
        intermediate_layer (tensor): intermediate layer for feature matching

    """
    layers = spectral_norm if do_spectral_norm else tf.layers

    with tf.compat.v1.variable_scope('discriminator_zz', reuse=reuse,
                           custom_getter=getter):

        y = tf.concat([z, rec_z], axis=-1)

        name_net = 'y_layer_1'
        with tf.compat.v1.variable_scope(name_net):
            y = layers.dense(y, units=64, kernel_initializer=init_kernel,
                                 name='fc')

            y = leakyReLu(y)
            y = tf.layers.dropout(y, rate=0.2, training=is_training,
                                  name='dropout')

        name_net = 'y_layer_2'
        with tf.compat.v1.variable_scope(name_net):
            y = layers.dense(y,
                                 units=32,
                                 kernel_initializer=init_kernel,
                                 name='fc')

            y = leakyReLu(y)
            y = tf.layers.dropout(y, rate=0.2, training=is_training,
                                  name='dropout')

        intermediate_layer = y

        name_net = 'y_layer_3'
        with tf.compat.v1.variable_scope(name_net):
            y = tf.layers.dense(y,
                                 units=1,
                                 kernel_initializer=init_kernel,
                                 name='fc')

            logits = tf.squeeze(y)

    return logits, intermediate_layer
