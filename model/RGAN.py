import numpy as np
import numpy.linalg as nplin
import tensorflow as tf
import importlib

class RGAN(object):
    """
    Des:
        X = L + S
        L is a non-linearly low rank matrix and S is a sparse matrix.
        argmin ||L - Decoder(Encoder(L))|| + ||S||_1
        Use Alternating projection to train model
    """
    def __init__(self, sess, logdir, enable_early_stop,
        dataset='mnist',
        norm_s='L1',
        lambda_=1.0,
        error = 1.0e-7):
        """
        sess: a Tensorflow tf.Session object
        layers_sizes: a list that contain the deep ae layer sizes, including the input layer
        lambda_: tuning the weight of l1 penalty of S
        error: converge criterior for jump out training iteration
        """
        self.lambda_ = lambda_
        self.logdir = logdir
        self.error = error
        self.errors=[]
        self.norm_s = norm_s
        self.enable_early_stop = enable_early_stop
        BiGAN = importlib.import_module('model.BasicBiGAN.BiGAN')
        self.BiGAN = BiGAN.BiGAN(sess=sess, dataset=dataset,
            logdir=self.logdir, enable_early_stop=enable_early_stop)

    def fit(self, sess, X, inner_iteration = 50,
            iteration=20, verbose=False):
        ## The first layer must be the input layer, so they should have same sizes.

        ## initialize L, S, mu(shrinkage operator)
        self.L = np.zeros(X.shape)
        self.S = np.zeros(X.shape)

        mu = (X.size) / (4.0 * nplin.norm(X,1))
        print ("shrink parameter:", self.lambda_ / mu)
        LS0 = self.L + self.S

        XFnorm = nplin.norm(X,'fro')
        if verbose:
            print ("X shape: ", X.shape)
            print ("L shape: ", self.L.shape)
            print ("S shape: ", self.S.shape)
            print ("mu: ", mu)
            print ("XFnorm: ", XFnorm)

        for it in range(iteration):
            if verbose:
                print ("Out iteration: " , it)
            ## alternating project, first project to L
            self.L = X - self.S
            ## Using L to train the auto-encoder
            self.BiGAN.fit(sess=sess,
                           X=self.L,
                           nb_epochs = inner_iteration,
                           verbose = verbose)
            # Restore from disk to get the best model
            if self.enable_early_stop:
                self.BiGAN.restore(sess)
            ## get optmized L
            self.L = self.BiGAN.getRecon(X = self.L, sess = sess)
            print("size L {}".format(self.L.shape))
            ## alternating project, now project to S
            if self.norm_s == 'L1':
                SHR = importlib.import_module('model.shrink.l1shrink')
                self.S = SHR.shrink(self.lambda_/mu,
                    (X - self.L).reshape(X.size)).reshape(X.shape)
            elif self.norm_s == 'L21':
                SHR = importlib.import_module('model.shrink.l21shrink')
                self.S = SHR.l21shrink(self.lambda_, (X - self.L))
            else: # 'L21ST'
                SHR = importlib.import_module('model.shrink.l21shrink')
                self.S = SHR.l21shrink(self.lambda_, (X - self.L).T).T

            ## break criterion 1: the L and S are close enough to X
            c1 = nplin.norm(X - self.L - self.S, 'fro') / XFnorm
            ## break criterion 2: there is no changes for L and S
            c2 = np.min([mu,np.sqrt(mu)]) * nplin.norm(LS0 - self.L - self.S) / XFnorm

            if verbose:
                print ("c1: ", c1)
                print ("c2: ", c2)

            if c1 < self.error and c2 < self.error :
                print ("early break")
                break
            ## save L + S for c2 check in the next iteration
            LS0 = self.L + self.S
            #print("L.shape={} S.shape={}".format(self.L.shape, self.S.shape))

        return self.L , self.S

    def restore(self, sess):
        self.BiGAN.restore(sess)

    def transform(self, X, sess):
        """Get hidden_z
        """
        L = X - self.S
        return self.BiGAN.transform(sess = sess, X=L)

    def get_sparse(self, X, sess):
        # S = X - L
        S = X - self.getRecon(X, sess)
        #l1S = [nplin.norm(S[i], 1) for i in range(S.shape[0])]
        l1S = np.sum(np.abs(S), axis=-1) # much faster
        return S, l1S

    def getRecon(self, X, sess):
        return self.BiGAN.getRecon(sess = sess, X=X)

    def evaluate(self, testx, sess):
        return self.BiGAN.evaluate(sess=sess, testx=testx)

if __name__ == "__main__":
    x = np.load(r"../data/data.npk", allow_pickle=True)[:500]
    logdir = "train_logs/MNIST4/"

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        rgan = RGAN(sess=sess,lambda_=2000, layers_sizes=[400, 784],
            logdir=logdir)
        L, S = rgan.fit(sess=sess,X=x, inner_iteration=30, iteration=5, verbose=True)
        recon_rgan = rgan.getRecon(sess=sess, X=x)
        print ("cost errors, not used for now:", rgan.errors)
    from collections import Counter
    print ("number of zero values in S:", Counter(S.reshape(S.size))[0])
