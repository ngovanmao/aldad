"""

SWAT GAN-LSTM architecture

Generator (decoder), encoder and disciminator.

"""
import tensorflow as tf
from utils import sn
from utils.mod_core_rnn_cell_impl import LSTMCell  # modified to allow initializing bias in lstm

learning_rate = 1e-5
batch_size = 500
latent_dim = 32
hidden_units_g = 100
hidden_units_d = 100
num_generated_features = 5 # 5 Principle components
learn_scale = False
seq_length = 120
seq_step = 10

def encoder(x_inp, is_training=False):
    pass

def generator(z_inp, is_training=False, getter=None, reuse=False):
    """ Generator architecture in Tensorflow
    Generate data from the latent space

    Args:

    Returns:
        net (tensor): last activation layer of the generator

    """
    with tf.variable_scope('generator', reuse=reuse, custom_getter=getter):
        W_out_G_initializer = tf.truncated_normal_initializer()
        b_out_G_initializer = tf.truncated_normal_initializer()
        scale_out_G_initializer = tf.constant_initializer(value=1.0)
        lstm_initializer = None
        bias_start = 1.0

        W_out_G = tf.get_variable(name='W_out_G',
            shape=[hidden_units_g, num_generated_features],
            initializer=W_out_G_initializer)
        b_out_G = tf.get_variable(name='b_out_G',
            shape=num_generated_features,
            initializer=b_out_G_initializer)
        scale_out_G = tf.get_variable(name='scale_out_G',
            shape=1,
            initializer=scale_out_G_initializer,
            trainable=learn_scale)

        # input
        inputs = z_inp

        cell = LSTMCell(num_units=hidden_units_g,
                        state_is_tuple=True,
                        initializer=lstm_initializer,
                        bias_start=bias_start,
                        reuse=reuse)
        rnn_outputs, rnn_stats = tf.nn.dynamic_rnn(
            cell=cell,
            dtype=tf.float32,
            sequence_length=[seq_length] * batch_size,
            inputs=inputs)
        print('rnn_outputs.shape={}'.format(tf.shape(rnn_outputs)))
        rnn_outputs_2d = tf.reshape(rnn_outputs, [-1, hidden_units_g])
        #logits_2d = tf.matmul(rnn_outputs_2d, W_out_G) + b_out_G
        logits_2d = tf.nn.xw_plus_b(rnn_outputs_2d, W_out_G, b_out_G)

        output_2d = tf.nn.tanh(logits_2d)
        output_3d = tf.reshape(output_2d,
            [-1, seq_length, num_generated_features])

    return output_3d


def discriminator(x_inp, is_training=False, getter=None, reuse=False):
    """
    Returns:
        - activation value (probability [0,1])
        - intermediate layer weighted sum
    """
    with tf.variable_scope('discriminator', reuse=reuse, custom_getter=getter) as scope:
        if reuse:
            scope.reuse_variables()
        W_out_D = tf.get_variable(name='W_out_D',
                                  shape=[hidden_units_d, 1],
                                  initializer=tf.truncated_normal_initializer())
        b_out_D = tf.get_variable(name='b_out_D',
                                  shape=1,
                                  initializer=tf.truncated_normal_initializer())

        inputs = x_inp

        # add the average of the inputs to the inputs (mode collapse)
        """
        if batch_mean:
            mean_over_batch =
            inputs = tf.concat(...
        """
        cell = tf.contrib.rnn.LSTMCell(num_units=hidden_units_d,
                                       state_is_tuple=True,
                                       reuse=reuse)
        rnn_outputs, rnn_state = tf.nn.dynamic_rnn(
            cell=cell,
            dtype=tf.float32,
            inputs=inputs)
        # output weighted sum
        print('rnn_outputs discriminator.shape={}'.format(tf.shape(rnn_outputs)))
        logits = tf.einsum('ijk,km', rnn_outputs, W_out_D) + b_out_D

        output = tf.nn.sigmoid(logits)
    return output, logits



